
# gl-utils

### ToDo

- Documentation
- Add `search` docs.gitlab.com/ee/api/search.html

Utility to prepare GitLab Projects for CI building. 

Goes hand-in-hand with Flying Hippo's `wordpress-starter` templates. 

Goes hand-in-hand with Flying Hippo's `packagistbuilder` system.



Examples:


Verify `master` branch protection status on all projects within a group

```bash
# Get json for all projects in group
gl --group fh-plugins group_projects > fh-plugins_projects.json

# Check branch protection status, reporting whether or not 
while read -r i; do 
  gl --quiet --project "$i" check_branch_protection master; 
done < <(cat fh-plugins_projects.json | jq -r '.[].path_with_namespace' | sort)
```


```bash
# Get json for all projects in group
gl --group fh-client-sites group_projects > fh-client-sites_projects.json

# Check branch protection status, reporting whether or not 
while read -r i; do 
  echo "$i"
  echo -n "   check_branch_protection 40 30......."
  if gl --quiet --project "$i" check_branch_protection master 40 30 >/dev/null; then
    echo "Ok"
  else
    gl --quiet --project "$i" configure_branch_protection master 40 30 >/dev/null && echo "Set" || echo "Error"
  fi
  echo -n "   check-merge_method ff..............."
  if gl --quiet --project "$i" check_merge_method ff >/dev/null; then
    echo "Ok"
  else
  	gl --quiet --project "$i" configure_merge_method ff >/dev/null && echo "Set" || echo "Error"
  fi
done < <(cat fh-client-sites_projects.json | jq -r '.[].path_with_namespace' | sort | head -n1)
```

```bash
# Get json for all projects in group
gl --group fh-client-sites group_projects > fh-client-sites_projects.json

# Search for occurances of fhrc-components
while read -r i; do 
  echo "$i"
  gl --quiet --project "$i" search fhrc-components
done < <(cat fh-client-sites_projects.json | jq -r '.[].path_with_namespace' | sort)
```