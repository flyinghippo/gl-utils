#!/bin/bash

code=(
	SC2128 #: Expanding an array without an index only gives the first element
	# SC2183 #: This format string has 2 variables, but is passed 1 arguments.
	# SC2059 #: Don't use variables in the printf format string. Use printf '..%s..' "$foo".
	SC1091 #: Not following: .env was not specified as input (see shellcheck -x).
  SC2034 #: OPT_FIX appears unused. Verify use (or export if used externally)
)
codes="$(printf '%s,' "${code[@]}")"
shellcheck --shell=bash --exclude="${codes%?}" gl
